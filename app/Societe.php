<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Societe extends Model {

	protected $table = "societes";
	protected $softDelete = true;

	public function Personnels() {
		return $this->hasMany('App\User');
	}
	public function Formations() {
		return $this->belongsToMany('App\Formation', 'societe_formation')->withPivot('quota');

	}

	public function Responsables() {
		return $this->hasMany('App\User');

	}
}
