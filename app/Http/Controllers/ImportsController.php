<?php

namespace App\Http\Controllers;

use App\Formation;
use App\Http\Requests\UsersRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Hash;

class ImportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except' => ['createPassword', 'initialisePassword']]);
    }

    public function sendMailsOrSms($user){

            $param = str_random(3) . $user->id . str_random(3);
            if ($user->email != null  && ($user->role != "superAdmin")) {
                Mail::send('Back.emails.contact', ['user' => $user, 'id' => $param], function ($message) use ($user) {
                    $message->from('pfefacebookmanager@gmail.com');
                    $message->to($user->email);
                    $message->subject('test');

                });

            }
            elseif(($user->tel !== null) &&(($user->email == null) || ($user->email == " "))&&($user->role!="superAdmin")) {
                $this->veriferTel($user->id);
                echo("sms envoyé en attendant l api");
                $key = "7691a9cc-3c29-4e9d-8f4a-e4d3ed17e4e6";
                $secret = "v4cRbFsOQkSVbnme8nj6Tw==";
                $phone_number = $user->tel;
                $user = "application\\" . $key . ":" . $secret;
                $message = array("message"=>"Test");
                $data = json_encode($message);
                $ch = curl_init('https://messagingapi.sinch.com/v1/sms/' . $phone_number);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_USERPWD,$user);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                $result = curl_exec($ch);

                if(curl_errno($ch)) {
                    echo 'Curl error: ' . curl_error($ch);
                } else {
                    echo $result;
                }
                curl_close($ch);
            }
        }


    public function createPassword($param,$id){

        $user = User::find($id);
        return view('Front.Personnel.initialisePwd',compact('user'));
    }
    public function initialisePassword(UsersRequest $request,$id){
        $user= User::find($id);
        if($user->password == null) {
            $user->password = Hash::make($request->password);
            $user->update();
        }
        else{
            "vous avez dèja une mot de passe";
        }
        return Redirect::to('/login');
    }
    public function index($id){
        $formation = Formation::with('Personnels')->findOrFail($id);
        $users = User::all()->where('role','!=','superAdmin');
        return view ('Back.User.index',compact('users','formation'));
    }

    public function import(Request $request,$id)
    {

        $formation = Formation::find($id);
        if ($request->file('imported-file')) {

            $path = $request->file('imported-file')->getRealPath();
            $data = Excel::load($path, function ($reader) {
            }
            )->get();
            if (!empty($data) && $data->count()) {
                if (($handle = fopen($path, "r")) !== FALSE) {
                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        //var_dump($data);
                        $dataArray[] = ['nom' => $data[0], 'email' => $data[1], 'tel' => $data[2]];
                    }
                    //die();
                    fclose($handle);
                }
            }
            if (!empty($dataArray)) {
                if ($dataArray[0] !== ['nom' => 'nom', 'email' => 'email', 'tel' => 'tel']) {
                    Session::flash('error', 'Format du fichier csv invalid ');
                    return \redirect()->back();
                }
                foreach ($dataArray as $d) {
                    if ($d['nom'] == null) {
                        Session::flash('error', 'Nom d\'utilisateur obligatoire');
                        return redirect()->back();
                    }
                    if (($d['email'] == null) && ($d['tel'] == null) && $d['nom'] !== null) {
                        Session::flash('error', 'Vérifier votre Fichier email ou numero de téléphone doit être remplit');
                        return redirect()->back();
                    }
                }
            }
            foreach ($dataArray as $key => $da) {
                if ($key > 0) {
                    if ((!empty($da['email'])) && (!filter_var($da['email'], FILTER_VALIDATE_EMAIL))) {
                        Session::flash('error', 'Invalid Email Format');
                        return redirect()->back();
                    }
                }
            }
            foreach ($dataArray as $items => $x) {
                if ($items > 0) {

                   // try {
                    if ( empty($x['tel']) ) { // will check for empty string, null values, see php.net about it
                        $tel = NULL;
                    } else {
                        $tel = $x['tel'];
                    }
                       $user =User::firstOrNew( ['email' => $x['email']], ['tel'=> $tel]);
                            $user->nom = $x['nom'];
                            $user->role = "personnel";
                            $user->societe_id = Auth::user()->societe_id;

                            $user->save();

                            $user->Formations()->sync($formation->id, false);

                        Session::flash('success', 'Utilisateurs ajoutés avec succés');

                 //   }
                   /* catch (\Exception $exception) {
                        Session::flash('error', 'erreur');
                    }*/
                    $this->sendMailsOrSms($user);
                }

            }
            return back();
        }
    }

    public function getDownload()
    {
        $file_path = public_path('uploads/exemple.csv');
        return response()->download($file_path);

    }

    public static function veriferTel($id) {
        $user=User::find($id);
        $tel = preg_replace('/\s+/', '', $user->tel);
        if(strlen($tel) > 9){
            if (strpos($tel, "+0033") === 0) {
                $tel = substr($tel, 5);
                dd($tel);
            } elseif (strpos($tel, "0033") === 0) {
                $tel = substr($tel, 4);
            }
        }
        if (ctype_digit($tel) && strlen($tel)== 9) {
            $tel = "0033" . $tel;
            return $tel;
        } else {
            return false;
        }
    }




}
