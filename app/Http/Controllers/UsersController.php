<?php

namespace App\Http\Controllers;

use App\Formation;
use App\Societe;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard(){
        return view('Back.dashboard');
    }

    public function indexResponsables(){
        $responsables = User::all()->where('role','=','responsable');
        return view('Back.User.indexResponsables',compact('responsables'));
    }


    public function store(Request $request){
        $this->validate($request,[
            'email'=>'required|unique:users|string|max:255',

            ]
        );
        $societe = Societe::all()->last();
        $user = new User();
        $user->nom = $request->nom;
        $user->email = $request->email;
        $user->role = "responsable";
        $user->tel = $request->tel;
        $user->password = Hash::make($request->password);
        $user->societe_id = $societe->id;
        $user->save();
        $data = $user->toArray();
        Mail::send('Back.emails.new', $data, function($message) use ($data){
            $message->from('pfefacebookmanager@gmail.com');
            $message->to($data['email']);
            $message->subject('Ajout Responsable');
        });
        return redirect()->back();
      //  return json_encode($societe);
    }

    public function update(Request $request,$id_societe){
        $societe = Societe::find($id_societe);
        $user = User::where('societe_id',$societe->id)->first();
        if($user !== null){

                $user->nom = $request->nom;
                $user->email = $request->email;
                $user->role = "responsable";
                $user->tel = $request->tel;
                $user->password = $request->password ? Hash::make($request->password) : $user->password;
                $user->societe_id  = $id_societe;
                $user->update();

            return redirect()->back();
            //return json_encode($user);
        }
        else{
            $user = new User();
            $user->nom = $request->nom;
            $user->email = $request->email;
            $user->role = "responsable";
            $user->tel = $request->tel;
            $user->password = Hash::make($request->password);
            $user->societe_id = $id_societe;
            $user->save();
            $data = $user->toArray();
            Mail::send('Back.emails.new', $data, function($message) use ($data){
                $message->from('pfefacebookmanager@gmail.com');
                $message->to($data['email']);
                $message->subject('Ajout Responsable');
            });
            return redirect()->back();
        }

    }
    public function delete($id){

            $user = User::find($id);
            $user->delete();
            return redirect()->back();
    }
}
