<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Choix extends Model {
	use SoftDeletes;

	protected $table = "choixes";
	protected $softDelete = true;

	public function Question() {
		return $this->belongsTo('App\Question');
	}
	public function Reponses() {
		return $this->hasMany('App\Reponse');
	}
}
