<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable {
	use Notifiable;
	use Notifiable;use SoftDeletes;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['nom', 'email', 'tel', 'password', 'image', 'role', 'societe_id'];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	public function setTelAttribute($value) {
		if (empty($value)) {
			// will check for empty string, null values, see php.net about it
			$this->attributes['tel'] = NULL;
		} else {
			$this->attributes['tel'] = $value;
		}
	}
	protected $softDelete = true;

	public function Formations() {
		return $this->belongsToMany('App\Formation', 'user_formation');
	}

	public function AdminFormations() {
		return $this->hasMany('App\Formation', 'user_id');
	}
	public function Lessons() {
		return $this->hasMany('App\Lesson', 'user_id');
	}
	public function Quizzes() {
		return $this->hasMany('App\Quiz', 'user_id');
	}
	public function Reponses() {
		return $this->hasMany('App\Reponse', 'user_id');
	}

	public function Lessonss() {
		return $this->belongsToMany('App\User', 'user_id');
	}
	public function Societe() {
		return $this->belongsTo('App\Societe');
	}

}
