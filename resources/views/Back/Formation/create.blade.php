@extends('Back.main')
@section('stylesheets')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" type="text/css">
    <link rel="stylesheet" href="{{asset('SmartExpFront/css/jquery-toast-min.css')}}" type="text/css">


    <style>
        .plus{
            width: 100px;
            height: 100px;
            background: red;
            margin-bottom: 15px;
        }
    </style>
@endsection
@section('content')

    <section class="content">
        <div class="box box-default">
            <div class="row">
                <div class="col-md-7"></div>
                <div class="col-md-5">
                    @include('Back.partials._messages')
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <h1 style="padding-left:10px">Ajouter une formation</h1>
                    {!! Form::open(array('route' => 'store.formation', 'data-parsley-validate' => '','id'=>'ajout-formation')) !!}
                    <div class="box-body">

                        <div class="form-group">
                            <label >Titre :</label>

                            <input type="text" class="form-control" placeholder="Titre" name="titre" id="titre">
                        </div>



                    </div>
                    <div class="box-footer" style="float:right">
                        <a class="btn btn-warning" href="{{ route('index.formation') }}">
                            Retour à la liste
                        </a>
                        <button type="submit" class="btn btn-success" id="btn-submit">Ajouter</button>
                    </div>
                    {!! Form::close() !!}
                </div>


                <div class="col-md-8">
                    <h1 style="padding-left: 10px;">Ajouter un leçon</h1>
                    <a data-toggle="modal" href="#myModal" class="btn btn-app" disabled="true" id="modal">
                        <i class="fa fa-plus"></i>
                    </a>
                </div>

                </div>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Leçon</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            {!! Form::open(array('route' => 'store.lesson', 'data-parsley-validate' => '','files' => true,'id' => 'ajout-lesson','method'=>'POST')) !!}
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Télécharger une video :</label>
                                    <input type="file" class="form-control" name="video" id="video">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <a class="btn btn-warning" href="{{ route('index.lesson') }}">
                                        Retour à la liste
                                    </a>
                                    <button type="submit" class="btn btn-success" id="submit-lesson">Ajouter</button>
                            </div>
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('javascripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('SmartExpFront/js/jquery-toast-min.js')}}"></script>

    <script type="text/javascript">



        $(document).ready(function() {
            $('.select2').select2();
        });


       $("#ajout-formation").submit(function (e) {
            e.preventDefault();
           var data = $(this).serialize();
           var url = $(this).attr('action');
           var form = $(this);
           var method = form.attr('method');
           var $btn = $('#btn-submit');
            $btn.button('loading');
           setTimeout(function () {
               $btn.button('reset');
           }, 1000);
            $.ajax({
                type: method,
                url: url,
                data: data,
                dataType: "json",
                success: function () {
                    $.toast({
                        heading: 'Succés',
                        text: 'Formation ajouté avec succées',
                        position: 'top-right',
                        loaderBg: 'green',
                        icon: 'success',
                        hideAfter: 3500,
                        stack: 6,
                        bgColor:'green'
                    });
                    $('#btn-submit').css('pointer-events','none');
                    $('#titre').val('');
                    $('#modal'). removeAttr('disabled');


                },
                error: function () {
                    $.toast({
                        heading: 'Erreur',
                        text: 'Vous devez ajouté 5 thèmes à l\'avance',
                        position: 'top-right',
                        loaderBg: '#FF0000',
                        icon: 'danger',
                        bgColor :'red',
                        hideAfter: 3500,
                        stack: 6
                    });
                    $('#titre').val('');
                }
            });
        });


        $("#ajout-lesson").submit(function (e) {
            e.preventDefault();
            var url = $(this).attr('action');
            var formData = new FormData(this);
            var $btn = $('#submit-lesson');
            $btn.button('loading');
            // simulating a timeout
            setTimeout(function () {
                $btn.button('reset');
            }, 1000);
            $.ajax({
                type: 'POST',
                url: url,
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",
                success: function () {
                    $('#myModal').modal('hide');

                    /*$(document).on("change", ".file_multi_video", function(evt) {
                        var $source = $('#video_here');
                        $source[0].src = URL.createObjectURL(this.files[0]);
                        $source.parent()[0].load();
                    });*/

                 //   $('#modal').load($('#modal'));

                    $.toast({
                        heading: 'Succés',
                        text: 'leçon ajouté avec succées',
                        position: 'top-right',
                        loaderBg: 'green',
                        icon: 'success',
                        hideAfter: 3500,
                        stack: 6,
                        bgColor:'green'
                    });
                    $('#submit-lesson').css('pointer-events','none');


                },
                error: function (data) {
                    $.toast({
                        heading: 'Erreur',
                        text: 'l\'ajout est echoué',
                        position: 'top-right',
                        loaderBg: '#FF0000',
                        icon: 'danger',
                        bgColor :'red',
                        hideAfter: 3500,
                        stack: 6
                    });

                }
            });
        });


    </script>



@endsection
