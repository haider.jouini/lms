<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SmartExpr @yield('title')</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Google Font -->
<link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<link rel="icon" type="image/png" href="{{asset('SmartExpFront/image/Favicon_smartxpr.png')}}">

<!-- Bootstrap 3.3.7 -->
{{Html::style('SmartExpBack/bower_components/bootstrap/dist/css/bootstrap.min.css')}}
<!-- Font Awesome -->
{{Html::style('SmartExpBack/bower_components/font-awesome/css/font-awesome.min.css')}}
<!-- Ionicons -->
{{Html::style('SmartExpBack/bower_components/Ionicons/css/ionicons.min.css')}}
<!-- jvectormap -->
{{Html::style('SmartExpBack/bower_components/jvectormap/jquery-jvectormap.css')}}
<!-- Theme style -->
{{Html::style('SmartExpBack/dist/css/AdminLTE.min.css')}}
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
{{Html::style('SmartExpBack/dist/css/skins/_all-skins.min.css')}}
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<script>
    window.Laravel =<?php echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>
</script>

<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
@yield('stylesheets')


