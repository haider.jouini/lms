<div class="modal modal-success fade" id="modal-success-{{$theme->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modifier thème</h4>
            </div>
            {!! Form::model($theme,['data-parsley-validate'=>'','route' => ['update.theme',$theme->id],'method'=>'PUT','role'=>'form']) !!}
            <div class="modal-body">
                <div class="form-group">
                    <label>Titre :</label>
                    <input type="text" class="form-control"  placeholder="Titre" name="titre" value="{{$theme->titre}}">
                </div>
                <div class="form-group">
                    <label>Description :</label>
                    {{Form::textarea('description',null,array('class'=>'form-control','required'=>'','maxlength'=>'255'))}}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" style="margin-right: 0px;">Modifier</button>
            </div>
            {!! Form::close() !!}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>