@extends('Back.main')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Liste des questions
        </h1>
    </section><br>
    <section>
        <a class="btn btn-primary" style="margin-left: 18px" href="{{ route('create.question') }}">
            Ajouter une question
        </a>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- /.col -->
            <!-- /.row -->
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>Libelle</th>
                                <th>Actions</th>
                            </tr>
                            @foreach($question as $question)
                                <tr>
                                    <td>{{$question->libelle}}</td>
                                    <td class="">
                                        {!! Form::open(['route' => ['delete.question', $question->id], 'method' => 'DELETE']) !!}
                                        <div class="dropdown">
                                            <button class="btn btn-default dropdown-toggle btn-sm" style="margin-bottom: 0px" type="button" data-toggle="dropdown">Actions
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <!-- Trigger the modal with a button
                                                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Supprimer</button>
                                                    -->
                                                    <a href="{{route('update.question',$question->id)}}" class="dropdown-item">Modifier</a>
                                                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#myModal{{$question->id}}">Supprimer</a>

                                                </li>
                                            </ul>
                                        </div>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                <div id="myModal{{$question->id}}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Supprimer</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Voulez vous vraiment supprimer la question : {{$question->libelle}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                <a href="{{route('delete.question',$question->id)}}" class="btn btn-primary" >Supprimer</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection