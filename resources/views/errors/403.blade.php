<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Smart Xpr</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" type="image/png" href="{{asset('SmartExpFront/image/Favicon_smartxpr.png')}}">
    <link rel="stylesheet" href="{{asset('SmartExpFront/css/login.css')}}">
    <link rel="stylesheet" href="{{asset('SmartExpFront/css/403.css')}}">
</head>
<body>
 <div class="container">
    <div class="component PC"></div>
    <div class="component connection">
      <div class="dot first"></div>
      <div class="dot second"></div>
      <div class="dot third"></div>
    </div>
    <div class="component server">
      <div class="slot"></div>
      <div class="slot"></div>
      <div class="button"></div>
      <div class="button"></div>
    </div>
    <button class="redirect " onclick="connect()">Retourner à la page d'acceuil</button>
 </div>

</body>
<script>
  function connect()
  {
    window.location.replace("/");
  }
  function reconnect()
  {
    setTimeout(function()
    {
      document.querySelector('.first').classList.toggle('num-four');
      document.querySelector('.second').classList.toggle('num-zero');
      document.querySelector('.third').classList.toggle('num-three');
      document.querySelector('.container').classList.toggle('_403');
      document.querySelector('.redirect').classList.toggle('visible');
    }, 1500);
  }
  window.addEventListener('load', reconnect);
</script>
</html>