<!doctype html>
<html>
    <head>
        <title>SmartXpr</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="icon" type="image/png" href="{{asset('SmartExpFront/image/Favicon_smartxpr.png')}}">
        <link rel="stylesheet" href="{{asset('SmartExpBack/bower_components/bootstrap/dist/css/bootstrap.min.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('SmartExpFront/css/login.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('SmartExpBack/dist/css/AdminLTE.min.css')}}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition">
        <div class="login-box" style="background: #fafafa;border-radius: 6px;width: 327px!important;">
        <!-- /.login-logo -->
            <div class="text-center">
                    <img src="{{asset('SmartExpFront/images/logo_smart_xpr.png')}}" style="padding: 39px 39px 8px 5px;">
            </div>
            <div>
               @yield('content')
               
            </div>

            <!-- /.social-auth-links -->
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous">
            
        </script>
        <script src="{{asset('SmartExpFront/bootstrap/js/bootstrap.js')}}"></script>
    </body>
</html>