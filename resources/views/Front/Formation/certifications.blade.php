@extends('Front.main')
@section('stylesheet')
    <style>
        .banner_area  a:active{
            border-bottom: 4px solid #ffffff;
        }
        a{color:#21c8fc !important;}

    </style>
@endsection

@section('content')
    <section class="banner_area">
        <div class="container">
            <div class="row" style="padding-top:18px">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h1 style="color: white;font-size: 32px !important;font-weight: 500 !important;">Certifications</h1>
                </div>
                <div class="col-md-6 text-right peronnalize-img" >
                    <img src="{{asset('SmartExpFront/images/logo-f.png')}}" >
                </div>
                <div class="col-md-6 ">
                    <ul id="menu" style=" margin-left: -64px;">
                        <li class="active"><a href="#" style="color: #ffffff">Accueil </a></li>

                        <li><a href="#"  style="color: #ffffff">profil</a></li>
                        <li><a href="#" style="color: #ffffff">Certifications</a></li>
                    </ul>
                </div>

            </div>

        </div>
    </section>
    <div class="content-wrapper" style="min-height: 901px;">
        <section class="formation_block_area section_gap">
            <div class="container h-100" >
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <h2 style="font-family: 'Roboto', sans-serif;font-size: 20px;color: #676767;font-weight: bold">Mes Certifications</h2>
                        <hr>
                        <div class="row">
                            <div class="col-md-4 col-12">
                                    <div class="card" style="margin-top: 35px">
                                        <div class="card-body">
                                            <h5 class="card-title" style="font-family: 'Roboto', sans-serif;font-size: 20px;color:#717171">Certification #1</h5>
                                            <hr>
                                            <p class="card-text" style="font-family: 'Roboto', sans-serif;font-size: 15px;color:#717171"><h5>Description: </h5> Lorem ipsum dolor  sit amet, pro in dolorem appete </p>
                                            <a href="#"><img src="{{asset('SmartExpFront/image/pdf.png')}}"> Certification01.pdf</a>
                                        </div>
                                    </div>
                            </div>

                            <div class="col-md-4 col-12">
                                    <div class="card" style="margin-top: 35px"  >
                                        <div class="card-body">
                                            <h5 class="card-title" style="font-family: 'Roboto', sans-serif;font-size: 20px;color:#717171">Certification #2</h5>
                                            <hr>
                                            <p class="card-text" style="font-family: 'Roboto', sans-serif;font-size: 15px;color:#717171"><h5>Description: </h5> Lorem ipsum dolor  sit amet, pro in dolorem appete </p>
                                            <a href="#"><img src="{{asset('SmartExpFront/image/pdf.png')}}"> Certification01.pdf</a>
                                        </div>
                                    </div>
                            </div>

                            <div class="col-md-4 col-12" >
                                    <div class="card" style="margin-top: 35px">
                                        <div class="card-body">
                                            <h5 class="card-title" style="font-family: 'Roboto', sans-serif;font-size: 20px;color:#717171">Certification #3</h5>
                                            <hr>
                                            <p class="card-text" style="font-family: 'Roboto', sans-serif;font-size: 15px;color:#717171"><h5>Description: </h5> Lorem ipsum dolor  sit amet, pro in dolorem appete </p>
                                            <a href="#"><img src="{{asset('SmartExpFront/image/pdf.png')}}"> Certification01.pdf</a>
                                        </div>
                                    </div>
                            </div>

                            <div class="col-md-4 col-12">
                                <div class="card" style="margin-top: 35px">
                                    <div class="card-body">
                                        <h5 class="card-title" style="font-family: 'Roboto', sans-serif;font-size: 20px;color:#717171">Certification #4</h5>
                                        <hr>
                                        <p class="card-text" style="font-family: 'Roboto', sans-serif;font-size: 15px;color:#717171"><h5>Description: </h5> Lorem ipsum dolor  sit amet, pro in dolorem appete </p>
                                        <a href="#"><img src="{{asset('SmartExpFront/image/pdf.png')}}"> Certification01.pdf</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    <hr>
                        <h5 style="font-size: 18px;font-family: 'Roboto', sans-serif;color: #676767">
                            Formations récemment achevées</h5>
                        <table class="table">
                            <thead style="background-color: #13c5fc;color: white">
                            <tr>
                                <th scope="col" style="font-family: 'Roboto', sans-serif;font-size: 15px">Formation</th>
                                <th scope="col" style="font-family: 'Roboto', sans-serif;font-size: 15px">Date Début</th>
                                <th scope="col" style="font-family: 'Roboto', sans-serif;font-size: 15px">Date Fin</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>

                                <td>Formation #001</td>
                                <td>22/06/2018</td>
                                <td>17/07/2018</td>
                                <td><a href="#">Reefaire</a></td>
                            </tr>
                            <tr>

                                <td>Formation #002</td>
                                <td>28/06/2018</td>
                                <td>20/07/2018</td>
                                <td><a href="#">Reefaire</a></td>
                            </tr>
                            <tr>

                                <td>Formation #003</td>
                                <td>22/07/2018</td>
                                <td>17/08/2018</td>
                                <td><a href="#">Reefaire</a></td>
                            </tr>
                            <tr>

                                <td>Formation #004</td>
                                <td>30/07/2018</td>
                                <td>17/09/2018</td>
                                <td><a href="#">Reefaire</a></td>
                            </tr>
                            </tbody>
                        </table>
                        <a href="{{route('home')}}" class="btn btn-primary" style="background-color: #d3d3d3;border: #d3d3d3;width: 173px;
    height: 45px;padding-top: 10px;color: white!important;"> <i class="fa fa-chevron-left"></i> Retour</a>
                    </div>

                </div>

            </div>
        </section>
    </div>
@endsection