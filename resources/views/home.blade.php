@extends('Front.main')
@section('stylesheets')
    <style>
        .banner_area  a:active{
            border-bottom: 4px solid #ffffff;
        }
    </style>
@endsection

@section('content')
    <section class="banner_area">
        <div class="container">
            <div class="row" style="padding-top:18px">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h1 style="color: white;font-size: 32px !important;font-weight: 500 !important;">Formations</h1>
                </div>
                <div class="col-md-6 text-right peronnalize-img" >
                    <img src="{{asset('SmartExpFront/images/logo-f.png')}}" >
                </div>
                <div class="col-md-6 ">
                    <ul id="menu" style=" margin-left: -64px;">
                        <li class="active"><a href="#" style="color: #ffffff">Tous </a></li>

                        <li><a href="#"  style="color: #ffffff">En Cours</a></li>
                        <li><a href="#" style="color: #ffffff">Terminé</a></li>
                    </ul>
                </div>

            </div>

        </div>
    </section>
    <div class="content-wrapper" style="min-height: 901px;">
    <section class="formation_block_area section_gap">
        <div class="container">
            <div class="row event_two">
                <div class="col-lg-3 col-sm-12 col-xs-12">
                    <div class="event_post">
                        <img src="{{asset('SmartExpFront/image/blog1.jpg')}}" alt="">
                       <h2 class="event_title">Animation d'équipe</h2>
                        <ul class="list_style sermons_category event_category">
                            <hr>
                            <li>Formateur : Nicolas Meunier</li>
                            <li>Durée : 10min</li>
                            <li>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" ></div>
                            </div>
                            </li>
                            <li class="text-center">
                                25% complete
                            </li>
                        </ul>
                        <a href="{{route('lesson')}}" class="btn_hover center-block">Continue</a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12 col-xs-12">
                    <div class="event_post">
                        <img src="{{asset('SmartExpFront/image/blog2.jpg')}}" alt="">
                        <h2 class="event_title">Animation d'équipe</h2>
                        <ul class="list_style sermons_category event_category">
                            <hr>
                            <li>Formateur : Nicolas Meunier</li>
                            <li>Durée : 10min</li>
                            <li>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar"></div>
                                </div>
                            </li>
                            <li class="text-center">
                                0% complete
                            </li>
                        </ul>
                        <a href="{{route('lesson')}}" class="btn_hover center-block">Accéder</a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12 col-xs-12">
                    <div class="event_post">
                        <img src="{{asset('SmartExpFront/image/blog3.jpg')}}" alt="">
                        <h2 class="event_title">Animation d'équipe</h2>
                        <ul class="list_style sermons_category event_category">
                            <hr>
                            <li>Formateur : Nicolas Meunier</li>
                            <li>Durée : 10min</li>
                            <li>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </li>
                            <li class="text-center">
                                100% complete
                            </li>
                        </ul>
                        <a href="{{route('lesson')}}" class="btn_hover center-block">Continue</a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12 col-xs-12">
                    <div class="event_post">
                        <img src="{{asset('SmartExpFront/image/blog3.jpg')}}" alt="">
                        <h2 class="event_title">Animation d'équipe</h2>
                        <ul class="list_style sermons_category event_category">
                            <hr>
                            <li>Formateur : Nicolas Meunier</li>
                            <li>Durée : 10min</li>
                            <li>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </li>
                            <li class="text-center">
                                25% complete
                            </li>
                        </ul>
                        <a href="{{route('lesson')}}" class="btn_hover center-block">Continue</a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12 col-xs-12">
                    <div class="event_post">
                        <img src="{{asset('SmartExpFront/image/blog1.jpg')}}" alt="">
                        <h2 class="event_title">Animation d'équipe</h2>
                        <ul class="list_style sermons_category event_category">
                            <hr>
                            <li>Formateur : Nicolas Meunier</li>
                            <li>Durée : 10min</li>
                            <li>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </li>
                            <li class="text-center">
                                25% complete
                            </li>
                        </ul>
                        <a href="{{route('lesson')}}" class="btn_hover center-block">Continue</a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12 col-xs-12">
                    <div class="event_post">
                        <img src="{{asset('SmartExpFront/image/blog2.jpg')}}" alt="">
                        <h2 class="event_title">Animation d'équipe</h2>
                        <ul class="list_style sermons_category event_category">
                            <hr>
                            <li>Formateur : Nicolas Meunier</li>
                            <li>Durée : 10min</li>
                            <li>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </li>
                            <li class="text-center">
                                25% complete
                            </li>
                        </ul>
                        <a href="{{route('lesson')}}" class="btn_hover center-block">Continue</a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12 col-xs-12">
                    <div class="event_post">
                        <img src="{{asset('SmartExpFront/image/blog3.jpg')}}" alt="">
                        <h2 class="event_title">Animation d'équipe</h2>
                        <ul class="list_style sermons_category event_category">
                            <hr>
                            <li>Formateur : Nicolas Meunier</li>
                            <li>Durée : 10min</li>
                            <li>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </li>
                            <li class="text-center">
                                25% complete
                            </li>
                        </ul>
                        <a href="{{route('lesson')}}" class="btn_hover center-block">Continue</a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12 col-xs-12">
                    <div class="event_post">
                        <img src="{{asset('SmartExpFront/image/blog3.jpg')}}" alt="">
                        <h2 class="event_title">Animation d'équipe</h2>
                        <ul class="list_style sermons_category event_category">
                            <hr>
                            <li>Formateur : Nicolas Meunier</li>
                            <li>Durée : 10min</li>
                            <li>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </li>
                            <li class="text-center">
                                25% complete
                            </li>
                        </ul>
                        <a href="{{route('lesson')}}" class="btn_hover center-block">Continue</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
@endsection
